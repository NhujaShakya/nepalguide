    //
//  AreaCell.swift
//  Nepal Guide
//
//  Created by Nhuja on 16/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit
import SDWebImage

class AreaCell: UICollectionViewCell {
    @IBOutlet weak var areaImg: UIImageView!
    @IBOutlet weak var areaName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        areaImg.layer.cornerRadius = 10
    }
    
    func configCell(areas: Area){
//        if let url = URL(string: areas.image){
//        do{
//            let data = try Data(contentsOf: url)
//            self.areaImg.image = UIImage(data: data)
//        }catch let error{
//            print("Error: \(error.localizedDescription)")
//        }
//        }
        areaImg.sd_setImage(with: URL(string: areas.image))
        areaName.text = areas.name
    }
}


