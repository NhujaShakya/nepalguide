//
//  TempleCell.swift
//  Nepal Guide
//
//  Created by Nhuja on 16/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

class TempleCell: UICollectionViewCell {
    @IBOutlet weak var templeImg: UIImageView!
    @IBOutlet weak var templeName: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        templeImg.layer.cornerRadius = 10
    }
    
    func configCell(temple: Temple){
//        if let url = URL(string: temple.image){
//
//            do{
//                let data = try Data(contentsOf: url)
//                self.templeImg.image = UIImage(data: data)
//            }catch let error {
//                print("Error= \(error.localizedDescription)")
//            }
//        }
        templeImg.sd_setImage(with: URL(string: temple.image))
        templeName.text = temple.name
    }
}
