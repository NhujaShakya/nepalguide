//
//  ProvinceCell.swift
//  Nepal Guide
//
//  Created by Nhuja on 16/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

class ProvinceCell: UITableViewCell {
    @IBOutlet weak var provinceView: UIView!
    @IBOutlet weak var provinceNo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        provinceView.layer.cornerRadius = 10
    }

    func configProCell(province: Province){
        provinceView.backgroundColor = UIColor.colorFromHex(province.color)
        provinceNo.text = "Province No.\(province.number)"
    }
}

