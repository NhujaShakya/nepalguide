//
//  Areas.swift
//  Nepal Guide
//
//  Created by Nhuja on 15/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

struct Area : Codable{
    let name: String
    let image: String
    var temples: [Temple]
    
    enum CodingKeys: String, CodingKey {
        case name
        case image = "picture_url"
        case temples = "location"
    }
}
