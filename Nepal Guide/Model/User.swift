//
//  User.swift
//  Nepal Guide
//
//  Created by Nhuja on 21/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import Foundation

struct User {
    var fullName: String
    var email: String
    var password: String
    var country: String
    var gender: String
//    let toDo:[Temple]
    
}
