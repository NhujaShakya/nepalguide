//
//  Temples.swift
//  Nepal Guide
//
//  Created by Nhuja on 15/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

struct Temple: Codable{
    let name: String
    let image: String
    let detail: String //note, detail, _description, <prefix>Description. Eg. cnDescription
    
    enum CodingKeys: String, CodingKey {
        case name
        case image = "picture_url"
        case detail = "description"
    }
}
