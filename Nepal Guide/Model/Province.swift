//
//  District.swift
//  Nepal Guide
//
//  Created by Nhuja on 15/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

struct Province: Codable {
    let number: Int
    let color: String
    var areas: [Area]
    
    enum CodingKeys: String, CodingKey{
        case number = "id"
        case color
        case areas = "districts"
    }
}

