//
//  UserLoginApi.swift
//  Nepal Guide
//
//  Created by Nhuja on 4/7/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import Foundation
import UIKit


class UserLoginApi {

    func doLogin(email: String,password: String, completion: @escaping Completion){
        let url = URL(string: USER_LOGIN_URL)
        let session = URLSession.shared

        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let paramToSend: [String: String] = ["email": email ,"password": password ]

        guard let httpBody = try? JSONSerialization.data(withJSONObject: paramToSend, options: []) else{ return DispatchQueue.main.async {
                completion(false, "Failed")
            }
        }
        request.httpBody = httpBody

        session.dataTask(with: request as URLRequest ) { (data, response, error) in
            if let error = error{
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription)
                }
            }
            if let data = data{
                do{
                    let jsonAny = try JSONSerialization.jsonObject(with: data, options: [])
                    guard let json = jsonAny as? [String: Any] else{ return DispatchQueue.main.async {
                        completion(false, error.debugDescription)
                        } }
                    let status = json["status"] as? Bool ?? false
                    let message = json["message"] as? String ?? ""
                    UserDefaults.standard.set(json["token"], forKey: "userToken")
                    DispatchQueue.main.async {
                        completion(status,message)
                    }
                    
                }catch{
                    debugPrint(error.localizedDescription)
                    DispatchQueue.main.async {
                        completion(false, error.localizedDescription)
                    }
                }
            }
        }.resume()
    }
}


