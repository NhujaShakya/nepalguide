//
//  ProvinceApi.swift
//  Nepal Guide
//
//  Created by Nhuja on 19/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class ProvinceApi {
    
    
    func getProvinceAlamo(completion: @escaping CompletionBlock){
        
        guard let url = URL(string: PROVINCE_URL) else{ return }
        
        Alamofire.request(url).responseJSON { (response) in
            if let error = response.result.error {
                debugPrint(error.localizedDescription)
                
                DispatchQueue.main.async {
                    completion(false, error.localizedDescription, [])
                    return
                }
            }
            
            guard let data = response.data else { return DispatchQueue.main.async { completion(false,"Fail retrieving data", [])}}
            do {
                let json = try JSON(data: data)
                let person = self.parseProvince(json: json)
                
                DispatchQueue.main.async {
                    completion(true,"",person)
                }
            }catch {
                debugPrint(error.localizedDescription)
                DispatchQueue.main.async {
                completion(false, "Data not found", [])
                return
                }
            }
        }
    }
    // address, description, photographer, price
    
    private func parseProvince(json: JSON) -> [Province]{
        
        let provinces = json["data"].arrayValue
        var allProvice = [Province]()
        
        for province in provinces {
            let number = province["id"].intValue
            let color = province["color"].stringValue
            let area = province["districts"].arrayValue
            
            var _province = Province(number: number, color: color, areas: [])
            
            for areas in area {
                var _area: Area
                
                let areaName = areas["name"].stringValue
                let areaPicture = areas["picture_url_with_default_value"].stringValue
//                let areaPicture_data = urlDataConvert(picture: areaPicture)
                let areaTemple = areas["locations"].arrayValue
                
                _area = Area(name: areaName, image: areaPicture, temples: [])
                
                for temples in areaTemple {
                    var _temple: Temple
                    
                    let templeName = temples["name"].stringValue
                    let templePicture = temples["picture_url_with_default_value"].stringValue
//                    let templePicture_data = urlDataConvert(picture: templePicture)
                    let templeDetail = temples["description"].stringValue
                    
                    _temple = Temple(name: templeName, image: templePicture, detail: ("\(templeDetail)"))
                    
                    _area.temples.append(_temple)
                    
                    
//                    prov.append(contentsOf: [
//                        Province(number: name, color: UIColor.colorFromHex(color), areas: [
//                            Area(name: areaName, image: areaPicture, temples: [
//                                Temple(name: templeName, image: templePicture, detail: templeDetail)
//                                ])
//                            ])
//                        ])
                    
                }
                
                _province.areas.append(_area)
            }
            //end of area loop.
            // required areas:
            
            allProvice.append(_province)
        }
        
        return allProvice
    }
    
//    private func urlDataConvert(picture: String) -> Data{
//        if let url = URL(string: picture){
//            do{
//                let data = try Data(contentsOf: url)
//                return data
//            }catch let error{
//                print("Error= \(error)")
//                return Data()
//            }
//        }
//        return Data()
//    }
}
