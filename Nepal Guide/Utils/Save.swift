//
//  Defaults.swift
//  Nepal Guide
//
//  Created by Nhuja on 1/7/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import Foundation

class Save {
    
    static func setValue(value: Any?, forKey: String) {
        UserDefaults.standard.setValue(value, forKey: forKey)
    }
    
    static func isUserLoggedIn(value: Bool) {
        UserDefaults.standard.setValue(value, forKey: "isUserLoggedIn")
    }
    
    static func isUserLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "isUserLoggedIn")
    }
    
    static func hello() {
        print("hello")
    }
    
}
