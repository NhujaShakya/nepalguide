//
//  Constraints.swift
//  Nepal Guide
//
//  Created by Nhuja on 20/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit


//let URL_BASE = "https://demo8059979.mockable.io/"
//let PROVINCE_URL = "https://demo8059979.mockable.io/areas"

let URL_BASE = "http://hamroguide.com/public/api/v1/"
let PROVINCE_URL = "http://hamroguide.com/public/api/v1/areas"

let USER_LOGIN_URL = "http://hamroguide.com/public/api/v1/login"
let USER_REGISTER_URL = "http://hamroguide.com/public/api/v1/register"


//let USER_LOGIN_URL = "https://lijeshshakya.000webhostapp.com/api/v1/login"
//let USER_REGISTER_URL = "https://lijeshshakya.000webhostapp.com/api/v1/register"
var user = [User]()
/// Typealias for province response after completion
//typealias ProvinceResponseCompletion = ([Province]?) -> Void
typealias CompletionBlock = (_ success: Bool, _ message: String, _ provices: [Province]) -> ()
typealias Parameter = [String: String]
typealias Completion = (_ success: Bool, _ message: String) -> ()
typealias CompletionHandler = (_ status: Bool, _ message: String) -> ()
