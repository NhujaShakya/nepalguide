//
//  TempleSelected.swift
//  Nepal Guide
//
//  Created by Nhuja on 17/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit
import AVKit

class TempleSelectedVC: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    //MARK:- IBOutlets
    @IBOutlet weak var templeImg: UIImageView!
    @IBOutlet weak var templeName: UILabel!
    @IBOutlet weak var templeDescription: UILabel!
    
    //    MARK:- public properties
    var temple: Temple!
    
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
//        if let url = URL(string: temple.image){
//            do{
//                let data = try Data(contentsOf: url)
//                self.templeImg.image = UIImage(data: data)
//            }catch let error{
//                print("Error= \(error.localizedDescription)")
//            }
//        }
        //title = temple.templeName// simple way to change the title of navigation
        
        customTempleNavBar()
        initView()
    }
    
    //MARK:- My Methods
    func customTempleNavBar(){
        let nameTitle = UILabel()
        nameTitle.text = temple.name
        nameTitle.font = UIFont.boldSystemFont(ofSize: 23)
        nameTitle.textColor = .red
        navigationItem.titleView = nameTitle
    }
    
    func initView() {
        templeImg.sd_setImage(with: URL(string: temple.image))
        templeName.text = temple.name
        templeDescription.text = temple.detail
    }
}
