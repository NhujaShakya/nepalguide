//
//  TempleSelectionVC.swift
//  Nepal Guide
//
//  Created by Nhuja on 16/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

class TempleSelectionVC: UIViewController {
    
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var templeSearch: UISearchBar!
    
    
    //MARK:- private properties
    private var searchingModel: (searching: Bool, searchTemples: [Temple]) = (false, [])
    
    
    //MARK:- public properties
    var area: Area!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        customTemplesNavBar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedTemple = sender as? Temple {
            if let templeVC = segue.destination as? TempleSelectedVC{
                templeVC.temple = selectedTemple
            }
        }
    }
    
    //MARK:- My Methods
    func customTemplesNavBar(){
        let nameTitle = UILabel()
        nameTitle.text = area.name
        nameTitle.font = UIFont.boldSystemFont(ofSize: 23)
        nameTitle.textColor = .red
        navigationItem.titleView = nameTitle
    }
    
    func initSetup(){
        collectionView.delegate = self
        collectionView.dataSource = self
        templeSearch.delegate = self
        //        collectionView.backgroundColor = .black
    }
}


extension TempleSelectionVC: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width
        let cellDimesion = (width/2) - 15
        return CGSize(width: cellDimesion, height: cellDimesion)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchingModel.searching {
            return searchingModel.searchTemples.count
        }
        else {
            return area.temples.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tblCell = collectionView.dequeueReusableCell(withReuseIdentifier: "templeCell", for: indexPath) as! TempleCell
        if searchingModel.searching {
            tblCell.configCell(temple: searchingModel.searchTemples[indexPath.row])
        } else
        {
            tblCell.configCell(temple: area.temples[indexPath.row])
        }
        return tblCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if searchingModel.searching {
            performSegue(withIdentifier: "toTempleSelection", sender: searchingModel.searchTemples[indexPath.row])
        } else {
            performSegue(withIdentifier: "toTempleSelection", sender: area.temples[indexPath.row])
        }
    }
}

extension TempleSelectionVC: UISearchBarDelegate{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText2: String) {
        guard !searchText2.isEmpty else{
            searchingModel.searching = false
            searchingModel.searchTemples = []
            collectionView.reloadData()
            return
        }
        searchingModel.searching  = true
//            searchTemples = area.temples.filter({temp -> Bool in
//            return temp.name.contains(searchText2)
//        })
        searchingModel.searchTemples =  area.temples.filter({$0.name.contains(searchText2)})
        collectionView.reloadData()
    }
}
