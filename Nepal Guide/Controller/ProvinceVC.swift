//
//  ViewController.swift
//  Nepal Guide
//
//  Created by Nhuja on 15/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

class ProvinceVC: UIViewController {
    
    
    //MARK:- IBOUTLETS
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    private var provinces = [Province]()
    private var searching:Bool = false
    
    //MARK:- PRIVATE PROPERTIES
    
    private var categoryToPass: Int!
    private var searchProvince = [Province]()
    private var provinceApi = ProvinceApi()
    
    
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initialSetup()
        requestProviceAPI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedProvince = sender as? Province {
            if let provinceVC = segue.destination as? AreaSelectionVC {
                provinceVC.province = selectedProvince
            }
        }else{
            print("you haven't selected provice.")
        }
    }
    
    
    deinit {
        print("sucess de inteal=zed/..")
    }
    
    //MARK:- MY METHODS
    
    func requestProviceAPI() {
        provinceApi.getProvinceAlamo{ [weak self] (status, message, province) in
            if status{
                self?.provinces = province
                self?.tblView.reloadData()
            }else{
                let alert = UIAlertController(title: message, message: "Something is Wrong", preferredStyle: .alert)
                let action = UIAlertAction(title: "Proceed", style: .default)
                alert.addAction(action)
                self!.present(alert,animated: true)
            }
        }
    }
    
    func customHomeNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        
        let nameTitle = UILabel()
        nameTitle.text = "Attithi Devo Bhava"
        nameTitle.font = UIFont.boldSystemFont(ofSize: 23)
        nameTitle.textColor = .red
        navigationItem.titleView = nameTitle
    }
    
    private func initialSetup() {
        
        tblView.dataSource = self
        tblView.delegate = self
        searchBar.delegate = self
        //        self.tblView.separatorColor = .black
        self.tblView.separatorStyle = .none
        //        title = "Attithi Devo Bhava"
        customHomeNavBar()
    }
    
    
    @IBAction func logOutBtnClicked(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Do you want to LogOut", preferredStyle: .alert)
        let action = UIAlertAction(title: "Yes", style: .default) {action in
            Save.isUserLoggedIn(value: false)
//            UserDefaults.standard.set(false, forKey: "isUserLoggedIn")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.loginCheckAndShowVC()
        }
        
        let action2 = UIAlertAction(title: "No", style: .default){ action in
            print("Still Login")
        }
        
        alert.addAction(action)
        alert.addAction(action2)
        present(alert,animated: true)
    }
    
}

extension ProvinceVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return searchProvince.count
        } else {
            return provinces.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "provinceCell") as! ProvinceCell
        if searching {
            cell.configProCell(province: searchProvince[indexPath.row])
        } else {
            cell.configProCell(province: provinces[indexPath.row])
        }
        cell.selectionStyle = .none
        return cell
        
    }
    
    
}

extension ProvinceVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searching {
            performSegue(withIdentifier: "toProvinceSelection", sender: searchProvince[indexPath.row])
        } else {
            performSegue(withIdentifier: "toProvinceSelection", sender: provinces[indexPath.row])
        }
    }
}

extension ProvinceVC: UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else{
            searching = false
            searchProvince = []
            tblView.reloadData()
            return
        }
        searching = true
        searchProvince = provinces.filter({temp -> Bool in
            let number:String = " \(temp.number)"
            return number.contains(searchText)
        })
        tblView.reloadData()
    }
}
