//
//  SignupVC.swift
//  Nepal Guide
//
//  Created by Nhuja on 18/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit



class SignupVC: UIViewController {
    
    //MARK:- IBOUTLETS
    
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPassTF: UITextField!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var bckView: UIView!
    @IBOutlet weak var genderBtn: DLRadioButton!
    
    //MARK:- PRIVATE PROPERTIES
    
    private var gender = ""
    private var signup = UserRegisterApi()
    
    
    //MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK:- MY METHODS
    
    func initSetup(){
        view.setGradientColorBackground(colorOne: .universalPurple, colorTwo: .universalPurple)
        subView.layer.cornerRadius = 10
        bckView.setGradientColorBackground(colorOne: .universalDarkPurple, colorTwo: .universalPurple)
        textfieldInit()
    }
    
    func textfieldInit(){
        fullNameTF.delegate = self
        emailTF.delegate = self
        passwordTF.delegate = self
        confirmPassTF.delegate = self
        countryTF.delegate = self
    }
    
    func alertMessage(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alert.addAction(action)
        present(alert,animated: true)
    }
    
    @IBAction func genderBtnClicked(_ sender: DLRadioButton) {
        if sender.tag == 1 {
            gender = "m"
        } else if sender.tag == 2{
            gender = "f"
        } else {
            gender = "o"
        }
    }
    
    
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        let registerFullName = fullNameTF.text
        let registerEmail = emailTF.text
        let registerCountry = countryTF.text
        let registerPassword = passwordTF.text
        let registerConfirmPass = confirmPassTF.text
        
        if (registerFullName!.isEmpty || registerEmail!.isEmpty || registerPassword!.isEmpty || gender.isEmpty){
            alertMessage(message: "Enter all the Fields")
        } else {
            if registerPassword!.count < 8 {
                alertMessage(message: "Password must contain 8 characters")
            }else {
                if registerPassword != registerConfirmPass {
                    alertMessage(message: "Passwords doenot match")
                } else {
                    
                    
                    signup.doRegister(user: User(fullName: registerFullName ?? "", email: registerEmail ?? "", password: registerPassword ?? "", country: registerCountry ?? "", gender: gender)) { (status, message) in
                        if status{
                            let alert = UIAlertController(title: "Registration", message: message, preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default){ action in
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                appDelegate.loginCheckAndShowVC()
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                            alert.addAction(action)
                            self.present(alert,animated: true)
                        } else {
                            self.alertMessage(message: message)
                        }
                    }
                }
            }
        }
    }
}

extension SignupVC: UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
