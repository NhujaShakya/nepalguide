//
//  LoginVC.swift
//  Nepal Guide
//
//  Created by Nhuja on 17/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit
import Foundation

class LoginVC: UIViewController {
    
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.barStyle = .black
    }
    
    //MARK:- IBOUTLETS
    
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPass: UITextField!
    @IBOutlet weak var bckView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- PRIVATE PROPERTIES
    
    private var login = UserLoginApi()
    
    
    //MARK:- LIFE CYCLE
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initSetup()
        customNavBar()

//        UserDefaults.setValue("santoshMaharjan", forKey: "username")
//        UserDefaults.setValue("password123", forKey: "password")
//        
//        Save.withValue(value: "username", forKey: "Username")
//        Save.withValue(value: "password", forKey: "Password")
    }
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    
    //MARK:- MY METHODS
    
    
    func customNavBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func initSetup(){
        view.setGradientColorBackground(colorOne: .universalPurple, colorTwo: .universalPurple)
        bckView.setGradientColorBackground(colorOne: .universalDarkPurple, colorTwo: .universalPurple)
        subView.layer.cornerRadius = 10
        signinButton.layer.cornerRadius = 10
        let emailImage = UIImage(named: "email")
        addLeftImageTo(txtField: userEmail, andImage: emailImage!)
        let passwordImage = UIImage(named: "password")
        addLeftImageTo(txtField: userPass, andImage: passwordImage!)
        userPass.delegate = self
        userEmail.delegate = self
    }
    
    @IBAction func signinBtnClicked(_ sender: Any) {
//        let storedEmail = UserDefaults.standard.string(forKey: "userEmail")
//        let storedPass = UserDefaults.standard.string(forKey: "userPass")
        login.doLogin(email: userEmail.text ?? "", password: userPass.text ?? "") { (status, message) in
            if status {
                Save.isUserLoggedIn(value: true)
                //            UserDefaults.standard.set(true, forKey: "isUserLoggedIn")
                self.performSegue(withIdentifier: "toMainView", sender: self)
            }else {
                let alert = UIAlertController(title: "Incorrent", message: message, preferredStyle: .alert)
                let action = UIAlertAction(title: "Try Again", style: .default)
                alert.addAction(action)
                self.present(alert,animated: true)
            }
        }
    }
    
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        performSegue(withIdentifier: "toSignupView", sender: self)
    }
    
}
extension LoginVC: UITextFieldDelegate{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.scrollView.endEditing(true)
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.scrollView.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func addLeftImageTo(txtField: UITextField, andImage img: UIImage){
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
}


