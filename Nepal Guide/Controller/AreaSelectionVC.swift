//
//  AreaSelectionVC.swift
//  Nepal Guide
//
//  Created by Nhuja on 16/6/19.
//  Copyright © 2019 Nhuja. All rights reserved.
//

import UIKit

class AreaSelectionVC: UIViewController {
    
    
    //MARK:-IBOutlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var districtSearch: UISearchBar!
    
    //MARK:- PRIVATE PROPERTIES
    
    private var searchModel: (isSearching: Bool, area: [Area]) = (false, [])
    
    //MARK:- PUBLIC PROPERTIES
    
    var province: Province!
    
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        title = "Province No.\(province.number)"
        customAreaNavBar()
        initSetup()
        //searchAreas = province.areas
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedArea = sender as? Area {
            if let areaVC = segue.destination as? TempleSelectionVC{
                areaVC.area = selectedArea
            }
        }
    }
    
    
    //MARK:- MY METHOD
    
    func customAreaNavBar() {
        let nameTitle = UILabel()
        nameTitle.text = ("Province No.\(province.number)")
        nameTitle.font = UIFont.boldSystemFont(ofSize: 23)
        nameTitle.textColor = .red
        navigationItem.titleView = nameTitle
    }
    
    func initSetup(){
        collectionView.dataSource = self
        collectionView.delegate = self
        districtSearch.delegate = self
        //        collectionView.backgroundColor = .black
    }
    
}



extension AreaSelectionVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.bounds.width
        let cellDimesion = (width/2) - 15
        return CGSize(width: cellDimesion, height: cellDimesion)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchModel.isSearching {
            return searchModel.area.count
        }else{
            return province.areas.count
        }
        //return searchAreas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let tblCell = collectionView.dequeueReusableCell(withReuseIdentifier: "areaCell", for: indexPath) as! AreaCell
        
        if searchModel.isSearching {
            tblCell.configCell(areas: searchModel.area[indexPath.row])
        }else{
            tblCell.configCell(areas: province.areas[indexPath.row])
        }
        return tblCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if searchModel.isSearching{
            performSegue(withIdentifier: "toAreaSelection", sender: searchModel.area[indexPath.row])
        }
        else{
            performSegue(withIdentifier: "toAreaSelection", sender: province.areas[indexPath.row])
        }
    }

}

extension AreaSelectionVC: UISearchBarDelegate{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else{
            searchModel.isSearching = false
            searchModel.area = []
            collectionView.reloadData()
            return
        }
        
        // high order function..
        // this is longest way
//        searchAreas = province.areas.filter({area -> Bool in
//            return area.name.contains(searchText)
//        })
        // can be done this way as well.
        searchModel.isSearching = true
        searchModel.area = province.areas.filter({$0.name.contains(searchText)})
        collectionView.reloadData()
    }
}

